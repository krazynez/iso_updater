#!/usr/bin/env python3
import pycurl
import certifi
import os
from hashlib import sha256

filename=[]
_url=[]
base_url=[]
base_url_with_sha=[]
base_filename=[]

def prep():
    # Get urls from urls.txt
    try:
        with open('urls.txt', 'r') as url:
            for link in url:
                if link != "\n" and link[0:1] != '#':
                    link = link.replace('\n', '')
                    _url.append(link)
            url.close()
    except IOError:
        print('urls.txt does not exist!')
        userInput = input('Would you like to create it? y/n: ')
        if userInput.lower() == 'y' or userInput.lower() == 'yes':
            with open('urls.txt', 'w') as f:
                f.close()
        else:
            print('Okay Goodbye then.')
            exit()
    # get just the filename from each url
    for i in _url:
        _file=i.split('/')
        for f in _file:
            if f.endswith('.iso'):
                filename.append(f)
                base_filename.append(f[:-4])

    # curl shasums
    for i in range(0,len(_url)):
        base_url_with_sha.append(_url[i].strip(''.join(filename))+'SHA256SUMS')
        base_url.append(_url[i].strip(''.join(filename)))


    # Make iso directories
    for i in range(0, len(base_filename)):
        if not os.path.isdir(base_filename[i]):
            os.mkdir(base_filename[i])


def check_intgerity(fp=None, fn=None,f_hash=None):
    print(f'Checking intergity of {fn}. Please wait...\n')
    with open(f_hash, 'r') as f:
        _hash = f.readlines()
        f.close()
    for i in _hash:
        if '*' in i:
            i=i.replace('*', '')
        if fn == i.split()[1]:
            files_hash=i.split()[0]    
    with open(f"{fp}/{fn}", 'rb') as f:
        bytes = f.read()
        dl_file_hash = sha256(bytes).hexdigest()
        f.close()
    if dl_file_hash == files_hash:
        print(f'Inegerity of {fn} is still good, latest iso image is still valid\n')
    else:
        print('Inegerity check of file is not the same. Possibly an updated file?\n')


def download(fn=None):
    if fn is None:
        return print("Why is fn not set?")
    
    # curl the urls
    if fn == 'urls':
        j = 0
        while j < len(_url):
            if not os.path.isfile(f"{base_filename[j]}/{filename[j]}"):
                with open(f"{base_filename[j]}/{filename[j]}", 'wb') as f:
                    c = pycurl.Curl()
                    c.setopt(c.URL, _url[j])
                    c.setopt(c.USERAGENT, 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0')
                    c.setopt(c.FOLLOWLOCATION, True)
                    c.setopt(c.WRITEDATA, f)
                    c.setopt(c.CAINFO, certifi.where())
                    print(f"Downloading {base_filename[j]} please wait...\n")
                    c.perform()
                    c.close()
            else:
                check_intgerity(base_filename[j], f"{filename[j]}",f"{base_filename[j]}/SHA256SUMS")
                
            j = j + 1

    # Get SHASUMS
    elif fn == 'shasums':
        j = 0
        while j < len(_url):
            with open(f"{base_filename[j]}/SHA256SUMS", 'wb') as f:
                c = pycurl.Curl()
                c.setopt(c.URL, f"{base_url[j]}/SHA256SUMS")
                c.setopt(c.USERAGENT, 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0')
                c.setopt(c.FOLLOWLOCATION, True)
                c.setopt(c.WRITEDATA, f)
                c.setopt(c.CAINFO, certifi.where())
                print(f"Downloading lastest {base_filename[j]}/SHA256SUMS please wait...\n")
                c.perform()
                c.close()
                
            j = j + 1


def main():
    try:
        prep()
        download('shasums')
        download('urls')
    except Exception as e:
        print(f'ERROR!: {e}')

if __name__ == '__main__':
    main()
